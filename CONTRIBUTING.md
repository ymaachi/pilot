## Contributor license agreement

By submitting code as an individual you agree to the
[individual contributor license agreement](LICENSE).
By submitting code as an entity you agree to the
[corporate contributor license agreement](LICENSE).

All Documentation content that resides under the [doc/ directory](/doc) of this
repository is licensed under Apache 2.0 license.


## Contribute to pilot

[View the new documentation](https://gitlab.com/ymaachi/pilot) to find the latest information.

## Security vulnerability disclosure

This [documentation](doc/development/contributing/index.md#security-vulnerability-disclosure) has been moved.

## Code of Conduct

This [documentation](https://gitlab.com/ymaachi/pilot) has been moved.

## Closing policy for issues and merge requests

This [documentation](doc/development/contributing/index.md#closing-policy-for-issues-and-merge-requests) has been moved.

## Helping others

This [documentation](doc/development/contributing/index.md#helping-others) has been moved.

## I want to contribute!

[View the new documentation](https://gitlab.com/ymaachi/pilot) to find the latest information.

## Contribution Flow

This [documentation](doc/development/contributing/index.md) has been moved.

## Workflow labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Type labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Subject labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Team labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Release Scoping labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Priority labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Severity labels

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

#### Severity impact guidance

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Label for community contributors

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

## Implement design & UI elements

This [documentation](doc/development/contributing/design.md) has been moved.

## Issue tracker

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Issue triaging

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Feature proposals

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Issue tracker guidelines

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Issue weight

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Regression issues

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Technical and UX debt

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

### Stewardship

This [documentation](doc/development/contributing/issue_workflow.md) has been moved.

## Merge requests

This [documentation](doc/development/contributing/merge_request_workflow.md) has been moved.

### Merge request guidelines

This [documentation](doc/development/contributing/merge_request_workflow.md) has been moved.

### Contribution acceptance criteria

This [documentation](doc/development/contributing/merge_request_workflow.md) has been moved.

## Definition of done

This [documentation](doc/development/contributing/merge_request_workflow.md) has been moved.

## Style guides

This [documentation](doc/development/contributing/style_guides.md) has been moved.
